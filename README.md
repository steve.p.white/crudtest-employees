# Spring Boot/MVC - CRUD API Test

## Overview

This application provides a simple web interface to create update and delete employee records. Upon start-up, it loads a number of default user records (5) from an inlined CSV resource, in to the local file based DB.
The backend is written in Java 8 and based on Spring Boot MVC framework which provides a HATEOAS+HAL API to CRUD employees and access API metrics. The front-end is written using ReactJS/JSX and interacts with a subset of the API features. Dynamic API documentation is provided via Swagger UI and REST API metrics/health via Spring Actuator. Security has been enforced using Spring security BASIC HTTP authentication.

## Build Environment

### Clean Build (Vagrant)

This is not required to build the project, but it provides a clean isolated environment to do so utilising Vagrant & VirtualBox.
The `Vagrantfile` is configured to build a VM based on CentOS 7, containing OpenJDK-devel 8, Maven, Docker and git.

- Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads (currently v5.2.2)
- Install Vagrant x64: https://www.vagrantup.com/downloads.html (currently v2.0.1). Windows 7 hosts require WMF 5.1
- Add vagrant to the environment PATH. On Windows this is typically: C:\HashiCorp\Vagrant\bin, e.g from an admin DOS cmd: `setx /M PATH "%PATH%;C:\HashiCorp\Vagrant\bin"`
- Install vagrant plugins: `vagrant plugin install vagrant-vbguest vagrant-faster`
- Open up a terminal to the source code root and run: `vagrant up`
- To connect to the VM: `vagrant ssh`
- Enter `rr` (an alias) to drop to source code root (`/vagrant`). This is mapped to the source root directory on the host.
- Proceed with the build process below (see: Building)

### Linux/Other

Ensure the following are installed:

- Java JDK 8 (e.g openjdk-devel)
- Maven 3.5.x
- Docker 18.x, with a running docker daemon (only required if building the Docker image)

## Building

From the source root, to build the:

- Backend-end only with a pre-built inline front-end: `mvn clean install`. This can then be run stand-alone (see: Running)
- Front-end and backend: `mvn -Pfrontend -Pbackend clean install`

The front-end build produces HTML/JS files -> `./frontend/build/*`. Build dependencies such as npm/node are installed into `./target` as part of this process (see pom for exact versions).

The back-end build generates a jar including the front-end (~50MB) -> `./target/crudtest-1.x.x-SNAPSHOT.jar`

To build the docker image containing the full application: `mvn dockerfile:build`. This will use the local running docker daemon to build and install the image in to. 
When using Vagrant/VirtualBox a port is already forwarded to the VM docker daemon on TCP:2375.

## Running

As a stand-alone Java application:

1. Change to `./target`
1. Start the application with: `java -jar crudtest-1.x.x-SNAPSHOT.jar`

As a Docker image:

1. Run: `docker run -d -p 8080:8080 -P --name crudtest spw/crudtest:latest`
 
Once the application is running, browse to: `http://localhost:8080` and login with the user/pass = __admin/crudtest__

Data: H2 DB is used as the DB (local file) which persists to `./work/main.mv.db`. This location is a volume under Docker.

Logs:

- Standalone: `./logs/crudtest.log`
- Docker: From the docker host, `docker logs crudtest -f`

## Developing

### Back-end

This has been built using the [Spring Boot MVC Initializr](https://start.spring.io) targetting Java 8 & Spring Boot 1.5.9. 
A number of integration tests have been implemented to validate that the REST API methods function correctly.

The project has been configured to:

- Include Jetty instead of Tomcat (smaller RAM footprint)
- Use Spring security HTTP BASIC authentication (hard coded credentials currently)
- Provide a self documenting API using swagger (see: 'API Documentation' link in the GUI)
- Provide API metrics using Spring Actuator
- Log using Logback
- Provide a CRUD endpoint for employees (./employees) which currently supports: POST, PUT, GET, DELETE
- A custom error controller (/error) for generating server side errors as JSON.

The project uses Kotlin for the domain POJOs to cut down on getter/setter verbosity. 
The 'Employee' entity extends from a base 'Record' object which contain common fields. There is future scope to add additional entity types. The integration tests have also been built with this in mind.
 
To develop the back-end code:

- Import the code in to Eclipse as a maven project
- Install the Eclipse Kotlin plugin (see [http://marketplace.eclipse.org/content/kotlin-plugin-eclipse](this link))

To run the code in development mode: `mvn spring-boot:run -Drun.jvmArguments='-Dserver.port=8081'`. 
This will run spring boot on the test port, with live-reload for certain aspects of the code (excluding domain object and dependency changes).

### Front-end

The front-end was built using [Facebooks create-react-app](https://github.com/facebook/create-react-app) as a basis for development. This provided a simple minimilistic skeleton to populate with code HTML/JS/JSX.
This contains: Webpack, ReactJS, Bootstrap, Rest client with HATEOAS/HAL support

To develop the front-end, ensure that the backend API is running locally on test port 8081 (`mvn spring-boot:run`), then:

- Change to: `./frontend/`
- Run: `npm run start`
- Login to `http://localhost:8081` first for UI API calls to function correctly
- Browse to the UI `http://localhost:3000`
- Import the `./frontend` folder in to MS Visual Studio Code

Any saved code changes will be refreshed in the browser.

## Futures

### Back-end

- Implement checkstyle/PMD
- Enforce higher Jacoco test code coverage
- Batch update API for bulk loads of data
- OAuth2 authentication mechanism
- Additional roles (e.g readonly, employeeAdmin, etc...)
- Different DB support (NoSQL)
- Add load tests with Jmeter
- Implement websockets so clients can register for changes to DB entities
- Search records API
- Pull front-end code from repository instead of a local location (convert to multi-module project)

### Front-end

- Add as a maven multi-module project and output as a versioned aritfact
- A ReactJS login page
- React-router private routes to prevent clients from accessing pages when not logged in
- Consider using redux to hold state
- Record list to use the data contained in the get all response instead of following HAL self/href links. This doesn't scale with a large number of records.
- Implement websockets to receive live entity updates
- Front-end tests
- Search records capability

## Useful Links

Spring Boot Initializr: [https://start.spring.io/]()

Spring Boot Swagger: [http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api]()

Spring Boot Jetty Configuration: [https://docs.spring.io/spring-boot/docs/current/reference/html/howto-embedded-servlet-containers.html]()

Spring Boot Testing: [https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html]()

Eclipse Kotlin plugin: [http://marketplace.eclipse.org/content/kotlin-plugin-eclipse]()

Eclipse FindBugs plugin: [http://www.vogella.com/tutorials/Findbugs/article.html]()

Facebook create-react-app: [https://github.com/facebookincubator/create-react-app]()

Docker cheat-sheet: [https://github.com/wsargent/docker-cheat-sheet]()
