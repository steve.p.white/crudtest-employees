import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import ReactDOM from 'react-dom';
import client from './client';
import when from 'when';
import follow from './follow'; // function to hop multiple links by "rel"
//import stompClient from './websocket-listener';
//import Login from "./Login"; // TODO: Login form
import { Button } from 'react-bootstrap';

const root = '/api';

// The gray background
const backdropStyle = {
	position: 'fixed',
	top: 0,
	bottom: 0,
	left: 0,
	right: 0,
	backgroundColor: 'rgba(0,0,0,0.3)',
	padding: 50,
	zIndex: 9999
};

// The modal "window"
const modalStyle = {
	backgroundColor: '#fff',
	borderRadius: 5,
	maxWidth: 320,
	minHeight: 160,
	margin: '0 auto',
	padding: 20,
	paddingTop: 10,
	zIndex: 1
};

export class App extends Component {

	constructor(props) {
		super(props);
		this.state = { employees: [], attributes: [], page: 1, pageSize: 5, links: {}, createModalOpen: false };
		this.updatePageSize = this.updatePageSize.bind(this);
		this.onCreate = this.onCreate.bind(this);
		this.onUpdate = this.onUpdate.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
		this.refreshCurrentPage = this.refreshCurrentPage.bind(this);
		this.refreshAndGoToLastPage = this.refreshAndGoToLastPage.bind(this);
		this.handleServerResponse = this.handleServerResponse.bind(this);
	}


	loadFromServer(pageSize) {
		follow(client, root, [
			{ rel: 'employees', params: { size: pageSize } }]
		).then(employeeCollection => {
			console.log('GET: ', employeeCollection);
			return client({
				method: 'GET',
				path: employeeCollection.entity._links.profile.href,
				headers: { 'Accept': 'application/schema+json' }
			}).then(schema => {
				// Filter unneeded JSON Schema properties, like uri references and subtypes ($ref).
				Object.keys(schema.entity.properties).forEach(function (property) {
					if (schema.entity.properties[property].hasOwnProperty('format') &&
						schema.entity.properties[property].format === 'uri') {
						delete schema.entity.properties[property];
					}
					else if (schema.entity.properties[property].hasOwnProperty('$ref')) {
						delete schema.entity.properties[property];
					}
				});

				this.schema = schema.entity;
				this.links = employeeCollection.entity._links;
				return employeeCollection;
			});
		}).then(employeeCollection => {
			this.page = employeeCollection.entity.page;
			return employeeCollection.entity._embedded.employees.map(employee =>
				client({
					method: 'GET',
					path: employee._links.self.href
				})
			);
		}).then(employeePromises => {
			return when.all(employeePromises);
		}).then(employees => {
			this.setState({
				page: this.page,
				employees: employees,
				attributes: Object.keys(this.schema.properties),
				pageSize: pageSize,
				links: this.links
			});
		});
	}

	handleServerResponse(response, employee) {
		this.refreshCurrentPage();
		// console.log('record: ', employee);
		// console.log('apiResponse: ', response);

		var href = response.request.method + ':' + response.request.path;
		var responseCode = response.status.code;

		if (responseCode >= 200 && responseCode <= 300) {
			return;
		}

		if (responseCode === 403) {
			alert('ACCESS DENIED: You are not authorized to ' +
				href);
		} else if (responseCode === 412) {
			alert('DENIED: Unable to ' + href +
				'\n Your copy is stale.');
		} else if (responseCode) {
			alert('ERROR: Unable to ' + href + '\nServer returned: ' + response.status.code);
		}
	}

	onCreate(employee, newEmployee) {
		follow(client, root, ['employees']).done(response => {
			client({
				method: 'POST',
				path: response.entity._links.self.href,
				entity: newEmployee,
				headers: { 'Content-Type': 'application/json' }
			}).done(response => {
				this.handleServerResponse(response, newEmployee);
			}, response => {
				this.handleServerResponse(response, newEmployee);
			});
		})
	}

	onUpdate(employee, updatedEmployee) {
		client({
			method: 'PUT',
			path: employee.entity._links.self.href,
			entity: updatedEmployee,
			headers: {
				'Content-Type': 'application/json',
				'If-Match': employee.headers.Etag
			}
		}).done(response => {
			this.handleServerResponse(response, updatedEmployee);
		}, response => {
			this.handleServerResponse(response, updatedEmployee);
		});
	}

	onDelete(employee) {
		client({ method: 'DELETE', path: employee.entity._links.self.href }
		).done(response => { this.handleServerResponse(response, employee); },
			response => {
				this.handleServerResponse(response, employee);
			});
	}

	onNavigate(navUri) {
		client({
			method: 'GET',
			path: navUri
		}).then(employeeCollection => {
			this.links = employeeCollection.entity._links;
			this.page = employeeCollection.entity.page;

			return employeeCollection.entity._embedded.employees.map(employee =>
				client({
					method: 'GET',
					path: employee._links.self.href
				})
			);
		}).then(employeePromises => {
			return when.all(employeePromises);
		}).done(employees => {
			this.setState({
				page: this.page,
				employees: employees,
				attributes: Object.keys(this.schema.properties),
				pageSize: this.state.pageSize,
				links: this.links
			});
		});
	}

	updatePageSize(pageSize) {
		if (pageSize !== this.state.pageSize) {
			this.loadFromServer(pageSize);
		}
	}

	refreshAndGoToLastPage(message) {
		follow(client, root, [{
			rel: 'employees',
			params: { size: this.state.pageSize }
		}]).done(response => {
			if (response.entity._links.last !== undefined) {
				this.onNavigate(response.entity._links.last.href);
			} else {
				this.onNavigate(response.entity._links.self.href);
			}
		})
	}

	refreshCurrentPage(message) {
		follow(client, root, [{
			rel: 'employees',
			params: {
				size: this.state.pageSize,
				page: this.state.page.number
			}
		}]).then(employeeCollection => {
			this.links = employeeCollection.entity._links;
			this.page = employeeCollection.entity.page;

			return employeeCollection.entity._embedded.employees.map(employee => {
				return client({
					method: 'GET',
					path: employee._links.self.href
				})
			});
		}).then(employeePromises => {
			return when.all(employeePromises);
		}).then(employees => {
			this.setState({
				page: this.page,
				employees: employees,
				attributes: Object.keys(this.schema.properties),
				pageSize: this.state.pageSize,
				links: this.links
			});
		});
	}

	// Data push futures
	componentDidMount() {
		this.loadFromServer(this.state.pageSize);
		// stompClient.register([
		// 	{ route: '/topic/newEmployee', callback: this.refreshAndGoToLastPage },
		// 	{ route: '/topic/updateEmployee', callback: this.refreshCurrentPage },
		// 	{ route: '/topic/deleteEmployee', callback: this.refreshCurrentPage }
		// ]);
	}

	toggleCreateModal = () => {
		//	console.log('toggleCreateModal: ', this.state.createModalOpen);
		this.setState({
			createModalOpen: !this.state.createModalOpen
		});
	}

	render() {
		return (

			<div>
				<div id="title" className="text-center">
					<h1 className="title-gradient">CRUD API Test</h1>
					<p></p>
				</div>

				<CreateUpdateDialog isUpdate={false} /* Create record modal */
					attributes={this.state.attributes}
					onUpdate={this.onCreate}
					show={this.state.createModalOpen}
					onClose={this.toggleCreateModal} />

				<EmployeeList page={this.state.page}
					employees={this.state.employees}
					links={this.state.links}
					pageSize={this.state.pageSize}
					attributes={this.state.attributes}
					onNavigate={this.onNavigate}
					onUpdate={this.onUpdate}
					onDelete={this.onDelete}
					updatePageSize={this.updatePageSize}
					onToggleCreateModal={this.toggleCreateModal} />
			</div>
		)
	}
}

class CreateUpdateDialog extends React.Component {

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);

	}

	handleSubmit(e) {
		e.preventDefault();
		var updatedEmployee = {};

		this.props.attributes.forEach(attribute => {
			var domNode = ReactDOM.findDOMNode(this.refs[attribute])
			if (domNode != null) {
				console.log('findAttrib: ', attribute);
				console.log('updatedEmployee: ', updatedEmployee);
				updatedEmployee[attribute] = domNode.value.trim();
			}
		});
		this.props.onUpdate(this.props.employee, updatedEmployee);
		window.location = "#";
		this.props.onClose();
	}

	ignoreRecordAttribute = (attribute) => {
		return (attribute === 'recordId' || /time..*/.test(attribute) || attribute === '_links'); // hide id and time* fields
	}

	render() {
		// Render nothing if the "show" prop is false
		if (!this.props.show) {
			return null;
		}
		console.log('this.props.attributes: ', this.props.attributes);
		var inputs = this.props.attributes.map(attribute => {
			if (!this.ignoreRecordAttribute(attribute)) {
				var key;
				if (this.props.isUpdate) {
					var record = this.props.employee;
					var recordId = record.entity.recordId;
					key = recordId + '-' + this.props.employee.entity[attribute];
					var defaultVal = record.entity[attribute];

					return (<p key={key}>
						<input type="text" placeholder={attribute}
							defaultValue={defaultVal}
							ref={attribute}
							className="field" />
					</p>);
				} else {
					key = attribute;
					return (<p key={key}>
						<input type="text" placeholder={attribute}
							ref={attribute}
							className="field" />
					</p>);
				}
			} else {
				return null;
			}
		}
		);

		var record = this.props.employee;
		var isCreate = record == null;
		var buttonText;
		if (!isCreate) {
			buttonText = "Update";
		} else {
			buttonText = "Create";
		}

		var dialogId = "set-" + (record != null ? this.props.employee.entity._links.self.href : "new");
		var formId = "dataForm-" + (record != null ? record.entity.recordId : "new");

		return (
			<div className="backdrop" style={backdropStyle}>

				<div id={dialogId} className="modalDialog" style={modalStyle}>
					<div>
						<a href="#close" title="Close" className="close" onClick={this.props.onClose}>X</a>
						{!isCreate ?
							<h4 className="width:0px;">Update Employee - {this.props.employee.entity.recordId}</h4>
							:
							<h4 className="width:0px;">Create Employee</h4>
						}
						<form id={formId}>
							{inputs}
							<Button bsStyle="primary" onClick={this.handleSubmit}>{buttonText}</Button>
						</form>
					</div>
				</div>
			</div>
		)
	}
}

class EmployeeList extends React.Component {

	constructor(props) {
		super(props);
		this.handleNavFirst = this.handleNavFirst.bind(this);
		this.handleNavPrev = this.handleNavPrev.bind(this);
		this.handleNavNext = this.handleNavNext.bind(this);
		this.handleNavLast = this.handleNavLast.bind(this);
		this.handleInput = this.handleInput.bind(this);
	}

	handleInput(e) {
		e.preventDefault();
		var pageSize = ReactDOM.findDOMNode(this.refs.pageSize).value;
		if (/^[0-9]+$/.test(pageSize)) {
			console.log('pageSize: ', pageSize);
			this.props.updatePageSize(pageSize);
		} else {
			ReactDOM.findDOMNode(this.refs.pageSize).value = pageSize.substring(0, pageSize.length - 1);
		}
	}

	handleNavFirst(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.first.href);
	}

	handleNavPrev(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.prev.href);
	}

	handleNavNext(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.next.href);
	}

	handleNavLast(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.last.href);
	}

	render() {
		var pageInfo = this.props.page.hasOwnProperty("number") ?
			<h4>Employee Records - Page {this.props.page.number + 1} / {this.props.page.totalPages} ({this.props.page.totalElements})</h4> : null;

		var employees = this.props.employees.map(employee =>
			<Employee key={employee.entity._links.self.href}
				employee={employee}
				attributes={this.props.attributes}
				onUpdate={this.props.onUpdate}
				onDelete={this.props.onDelete} />
		);

		var navLinks = [];
		if ("first" in this.props.links) {
			navLinks.push(<Button key="first" bsStyle="primary" onClick={this.handleNavFirst}>&lt;&lt;</Button>);
		}
		var prevIsDisabled = true;
		if ("prev" in this.props.links) {
			prevIsDisabled = false;
		}

		navLinks.push(<Button key="prev" bsStyle="primary" disabled={prevIsDisabled} onClick={this.handleNavPrev}>&lt;</Button>);

		var nextIsDisabled = true;
		if ("next" in this.props.links) {
			nextIsDisabled = false;
		}
		navLinks.push(<Button key="next" bsStyle="primary" disabled={nextIsDisabled} onClick={this.handleNavNext}>&gt;</Button>);

		if ("last" in this.props.links) {
			navLinks.push(<Button key="last" bsStyle="primary" onClick={this.handleNavLast}>&gt;&gt;</Button>);
		}
	
		return (
			<div>
				<div className="col-lg-4"></div>

				<div className="col-lg-4">
					<div className="text-center" style={{ paddingBottom: 10 }} >
						<Button type="submit" id="create" name="create" value="Create" onClick={this.props.onToggleCreateModal}>Create Employee</Button>
					</div>
					{pageInfo}
					<table className="table">
						<tbody>
							<tr>
								<th>ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th style={{ textAlight: 'center' }}>Action</th>
							</tr>
							{employees}
						</tbody>
					</table>
					<div className='text-center'>
						{navLinks}
						<div style={{ textAlign: 'right' }} onInput={this.handleInput} >
							<select ref="pageSize">
								<option value="5">5</option>
								<option value="10">10</option>
								<option value="20">20</option>
								<option value="50">50</option>
								<option value="100">100</option>
								<option value="200">200</option>
							</select>
						</div>
						<hr style={{ width: 100 + '%', height: 1, paddingTop: 10 }} />
						API: <a href="swagger-ui.html">Documentation</a> / <a href="/health">Health</a> / <a href="/metrics">Metrics</a>
					</div>
				</div>
				<div className="col-lg-4"></div>
			</div>
		)
	}
}

class Employee extends React.Component {

	constructor(props) {
		super(props);
		this.handleDelete = this.handleDelete.bind(this);
		this.state = { updateModalOpen: false };
	}

	handleDelete() {
		this.props.onDelete(this.props.employee);
	}

	toggleUpdateModal = () => {
		this.setState({
			updateModalOpen: !this.state.updateModalOpen
		});
	}

	render() {
		return (
			<tr>
				<td>{this.props.employee.entity.recordId}</td>
				<td>{this.props.employee.entity.firstName}</td>
				<td>{this.props.employee.entity.lastName}</td>
				<td style={{ width: 158 + 'px' }}>
					<CreateUpdateDialog
						isUpdate={true}
						employee={this.props.employee}
						attributes={this.props.attributes}
						onUpdate={this.props.onUpdate}
						show={this.state.updateModalOpen}
						onClose={this.toggleUpdateModal} />
					<Button type="submit" id="update" name="update" value="Update" onClick={this.toggleUpdateModal}>Update</Button>
					<Button type="submit" id="delete" name="delete" value="Delete" onClick={this.handleDelete}>Delete</Button>
				</td>
			</tr>
		)
	}
}
