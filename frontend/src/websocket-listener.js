var SockJS = require('sockjs-client');
var Stomp = require('stompjs');

function register(registrations) {
	var socket = SockJS('/api/updates');
	var stompClient = Stomp.over(socket);
	stompClient.connect({}, function(frame) {
		registrations.forEach(function (registration) {
			stompClient.subscribe(registration.route, registration.callback);
		});
	});
}

module.exports = {
	register: register
};
