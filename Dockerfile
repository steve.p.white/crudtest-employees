FROM openjdk:8-jdk-alpine
ARG jarFile

MAINTAINER steve-white

VOLUME /work
VOLUME /logs

ADD ${jarFile} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","-Xmx512m", "-Xss1m","/app.jar"]

EXPOSE 8080
