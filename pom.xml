<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <artifactId>crudtest</artifactId>
  <version>1.0.2</version>
  <packaging>jar</packaging>

  <name>crudtest</name>
  <description>CRUD Test Application</description>

  <parent>
    <groupId>com.spw</groupId>
    <artifactId>parent</artifactId>
    <version>1.0.0</version>
    <relativePath>./parent</relativePath>
  </parent>

  <dependencies>
    <dependency>
      <groupId>org.jetbrains.kotlin</groupId>
      <artifactId>kotlin-stdlib-jre8</artifactId>
    </dependency>
    <dependency>
      <groupId>org.jetbrains.kotlin</groupId>
      <artifactId>kotlin-reflect</artifactId>
    </dependency>

    <!-- Spring -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
      <exclusions>
        <!-- Exclude Tomcat and use jetty (below) -->
        <exclusion>
          <groupId>org.springframework.boot</groupId>
          <artifactId>spring-boot-starter-tomcat</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-jetty</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-rest</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
    <dependency>
      <groupId>org.thymeleaf.extras</groupId>
      <artifactId>thymeleaf-extras-springsecurity4</artifactId>
    </dependency>

    <dependency>
      <!-- Spring Actuator monitoring/metrics -->
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <dependency>
      <groupId>io.springfox</groupId>
      <artifactId>springfox-swagger2</artifactId>
      <version>${springfox-swagger.version}</version>
    </dependency>
    <dependency>
      <groupId>io.springfox</groupId>
      <artifactId>springfox-swagger-ui</artifactId>
      <version>${springfox-swagger.version}</version>
    </dependency>

    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <scope>runtime</scope>
    </dependency>

    <!-- Commons -->
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-csv</artifactId>
      <version>${commons-csv.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>${commons-lang3.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-collections4</artifactId>
      <version>${commons-collections4.version}</version>
    </dependency>

    <!-- Test dependencies -->
    <dependency>
      <groupId>org.springframework.security</groupId>
      <artifactId>spring-security-test</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <!-- Query Java Pojos -->
      <groupId>commons-jxpath</groupId>
      <artifactId>commons-jxpath</artifactId>
      <version>${commons-jxpath.version}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <!-- Hamcrest regex matchers -->
      <groupId>com.jcabi</groupId>
      <artifactId>jcabi-matchers</artifactId>
      <scope>test</scope>
    </dependency>

    <!-- Provide SuppressFBWarnings annotation to ignore FindBug issues,
      per-class -->
    <dependency>
      <groupId>com.google.code.findbugs</groupId>
      <artifactId>annotations</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.google.code.findbugs</groupId>
      <artifactId>jsr305</artifactId>
      <scope>provided</scope>
    </dependency>
  </dependencies>

  <profiles>
    <profile>
      <id>backend</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
          </plugin>
          <plugin>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-maven-plugin</artifactId>
          </plugin>
          <plugin>
            <!-- Test code coverage enforce and reporting -->
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <executions>
              <!-- The check goal by runs in the verify phase - fail the
                build if mimimum code coverage checks aren't met -->
              <execution>
                <id>check</id>
                <configuration>
                  <rules>
                    <rule>
                      <excludes>
                        <exclude>com.spw.crudtest.config</exclude>
                        <exclude>com.spw.crudtest.CrudTestApplication</exclude>
                        <exclude>com.spw.crudtest.controller.ApiErrorController</exclude>
                        <exclude>com.spw.crudtest.ControllerInterceptor</exclude>
                        <exclude>com.spw.crudtest.domain</exclude>
                      </excludes>
                    </rule>
                  </rules>
                </configuration>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <!-- Enforce >= maven version -->
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-enforcer-plugin</artifactId>
          </plugin>
          <plugin>
            <!-- Copy the front-end components to the output directory -->
            <artifactId>maven-resources-plugin</artifactId>
            <executions>
              <execution>
                <id>copy-frontend</id>
                <phase>prepare-package</phase>
                <goals>
                  <goal>copy-resources</goal>
                </goals>
                <configuration>
                  <outputDirectory>${project.build.directory}/classes/static</outputDirectory>
                  <resources>
                    <resource>
                      <directory>${project.basedir}/frontend/build/</directory>
                      <includes>
                        <include>**/*</include>
                      </includes>
                    </resource>
                  </resources>
                </configuration>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <!-- Generate a ready-to-run docker image containing the
              application -->
            <groupId>com.spotify</groupId>
            <artifactId>dockerfile-maven-plugin</artifactId>
            <version>${dockerfile-maven.version}</version>
            <configuration>
              <repository>${docker.image.prefix}/${project.artifactId}</repository>
              <buildArgs>
                <jarFile>target/${project.build.finalName}.jar</jarFile>
              </buildArgs>
            </configuration>
          </plugin>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>findbugs-maven-plugin</artifactId>
          </plugin>
        </plugins>
      </build>
      <reporting>
        <plugins>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>findbugs-maven-plugin</artifactId>
          </plugin>
        </plugins>
      </reporting>
    </profile>
    <profile>
      <id>frontend</id>
      <build>
        <plugins>
          <plugin>
            <!-- Install node/npm in to local target folder and build the
              front-end -->
            <groupId>com.github.eirslett</groupId>
            <artifactId>frontend-maven-plugin</artifactId>
            <version>${frontend-maven.version}</version>
            <configuration>
              <installDirectory>target</installDirectory>
            </configuration>
            <executions>
              <execution>
                <id>install-node-and-npm</id>
                <goals>
                  <goal>install-node-and-npm</goal>
                </goals>
                <configuration>
                  <workingDirectory>frontend</workingDirectory>
                  <nodeVersion>v${node.version}</nodeVersion>
                  <npmVersion>${npm.version}</npmVersion>
                </configuration>
              </execution>
              <execution>
                <id>npm-install</id>
                <goals>
                  <goal>npm</goal>
                </goals>
                <configuration>
                  <workingDirectory>frontend</workingDirectory>
                  <arguments>install</arguments>
                </configuration>
              </execution>
              <execution>
                <id>npm build</id>
                <goals>
                  <goal>npm</goal>
                </goals>
                <configuration>
                  <workingDirectory>frontend</workingDirectory>
                  <arguments>run build</arguments>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
