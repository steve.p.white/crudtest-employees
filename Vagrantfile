# Check required plugins
REQUIRED_PLUGINS = %w(vagrant-vbguest)
exit unless REQUIRED_PLUGINS.all? do |plugin|
  Vagrant.has_plugin?(plugin) || (
    puts "The #{plugin} plugin is required. Please install it with:"
    puts "$ vagrant plugin install #{plugin}"
    false
  )
end

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  
  config.vm.hostname = "vmcrudtest"
   
  config.vm.provider "virtualbox" do |v|
	v.name = "dev-crudtest"
    v.memory = 1024
    v.cpus = 2
  end
  
  config.vm.network :forwarded_port, :host => 2375, :guest => 2375 # Docker daemon port
   
  (8080..8080).each do |port| # Spring boot ports (scope to add more if applicable)
    config.vm.network :forwarded_port, :host => port, :guest => port 
  end

  config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
  
  config.vm.provision "shell", inline: <<-SHELL
  
MVNVER=3.5.2
JAVAVER=1.8.0

BASHRC=/home/vagrant/.bashrc

cd ~/
sudo yum check-update
curl -fsSL https://get.docker.com/ | sh
sudo usermod -aG docker vagrant
sudo systemctl start docker
sudo systemctl status docker
sudo systemctl enable docker

sudo yum install -y net-tools bind-utils traceroute
sudo yum install -y epel-release
sudo yum install -y python-pip
sudo pip install docker-compose
sudo pip install --upgrade pip
sudo yum install -y screen wget socat ntpdate ntp telnet
sudo yum install -y java-$JAVAVER-openjdk java-$JAVAVER-openjdk-devel

sudo yum install -y httpd-tools # include the 'ab' Apache benchmarking tool: http://httpd.apache.org/docs/2.0/programs/ab.html

# Use WANdisco repo for Git 2.x
sudo rpm -ivh http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm
sudo yum install -y git

sudo bash -c "cd /opt/ ; curl http://mirror.ox.ac.uk/sites/rsync.apache.org/maven/maven-3/$MVNVER/binaries/apache-maven-$MVNVER-bin.tar.gz | tar -xvz"
echo "PATH=\$PATH:/opt/apache-maven-$MVNVER/bin/" >> $BASHRC

# Make docker daemon listen on TCP:2375
mkdir -p /etc/systemd/system/docker.service.d
cat << DOCKERSYSTEMD > /etc/systemd/system/docker.service.d/docker-external.conf

[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock

DOCKERSYSTEMD

systemctl daemon-reload
systemctl restart docker

# Set some shell helper variables
echo "export CODEROOT=\"/vagrant\"" >> $BASHRC
echo "alias rr=\\"cd \\$CODEROOT\\"" >> $BASHRC

SHELL

end
