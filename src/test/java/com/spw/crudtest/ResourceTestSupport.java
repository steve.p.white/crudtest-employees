package com.spw.crudtest;

import static com.jcabi.matchers.RegexMatchers.matchesPattern;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.TypeRef;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.spw.crudtest.DataLoader;
import com.spw.crudtest.CrudTestApplication;
import com.spw.crudtest.Util;
import com.spw.crudtest.client.ApiMediaTypes;
import com.spw.crudtest.client.ApiPageHandlerCallback;
import com.spw.crudtest.client.ApiRequestLoggingInterceptor;
import com.spw.crudtest.client.HalConstants;
import com.spw.crudtest.client.domain.HalPage;
import com.spw.crudtest.client.domain.HalResponse;
import com.spw.crudtest.client.domain.RecordJson;
import com.spw.crudtest.config.WebSecurityConfig;
import com.spw.crudtest.domain.Record;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathNotFoundException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class ResourceTestSupport {
    private static final Logger log = LoggerFactory.getLogger(ResourceTestSupport.class);

    protected RestTemplate restTemplate;

    @Autowired
    private CrudTestApplication app;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    protected TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;

    public static final String DEFAULT_USER = WebSecurityConfig.ADMIN_USER;
    public static final String DEFAULT_PASS = WebSecurityConfig.ADMIN_PASS;

    /**
     * Number of send threads to use when performing parallel POSTs
     */
    public static final int DEFAULT_POST_THREADS = 10;
    /**
     * Default Spring MVC REST API page size
     */
    public static final int DEFAULT_PAGE_SIZE = 20;

    public static final String URI_PREFIX = "http://localhost";
    public static final String API_PREFIX = "/api";

    /**
     * Common Jackson TypeReference used to map output to Map<String,Object>()
     */
    public static final TypeReference<Map<String, Object>> TYPEREF_MAP_STRING_OBJECT = new TypeReference<Map<String, Object>>() {
    };

    public static final Configuration JSONPATH_CONF = Configuration
            .builder()
            .mappingProvider(new JacksonMappingProvider())
            .jsonProvider(new JacksonJsonProvider())
            .build();

    ObjectMapper jacksonMapper;

    String resourceName; //  e.g: employees
    String entityName; // e.g: employee
    String resourceJsonPath;// JSON path used to get to the current resource on test from a list response
    Class<? extends Record> entityType; // entity type being tested. All entities currently extend Record (id, links)

    public long timeBefore; // store an initial 'timeUpdated' to validate the records loaded at startup have a timeUpdated > this.timeUpdated

    public ResourceTestSupport(Class<? extends Record> entityType) {
        if (entityType != null) { // TODO revisit and tidy up
            this.entityType = entityType;
            this.entityName = getApiEntityName(entityType);
            this.resourceName = getApiResourceName(entityType);
            this.resourceJsonPath = getJsonPathRootForResource(resourceName);
        }

        this.jacksonMapper = new ObjectMapper();
        this.jacksonMapper.setSerializationInclusion(Include.NON_NULL);
    }

    @PostConstruct
    public void setRestTemplate() {
        this.restTemplate = this.testRestTemplate.getRestTemplate();
        this.restTemplate.setInterceptors(getStandardApiInterceptors());
        this.timeBefore = this.app.getStartTime();
    }

    public List<ClientHttpRequestInterceptor> getStandardApiInterceptors() {
        return Arrays.asList(new ApiRequestLoggingInterceptor());
    }

    public Class<? extends Record> getEntityType() {
        return entityType;
    }

    public long getTimeBefore() {
        return this.timeBefore;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getResourceName() {
        return resourceName;
    }

    public ObjectMapper getJacksonMapper() {
        return jacksonMapper;
    }

    public String getUriBase() {
        return URI_PREFIX + ":" + port;
    }

    public String getJsonPathRootForResource(String resourceName) {
        return "$." + HalConstants.EMBEDDED + "." + resourceName;
    }

    public void validateBasicAuthOnEntityRestApi() {
        String uri = getUriBase() + getEntityUriPath(null);

        ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class); // no auth
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getHeaders().getContentType(), equalTo(ApiMediaTypes.TEXT_HTML_UTF8)); // validate that the returned data is the login form
    }

    public static void validateRestConstraintError(ResponseEntity<String> restResponse) {
        assertThat(restResponse.getStatusCode(), equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
        assertThat(restResponse.getHeaders().getContentType(), equalTo(MediaType.APPLICATION_JSON_UTF8));
    }

    public <T> T getEntityResult(MvcResult result, Class<T> clsType) throws JsonParseException, JsonMappingException, IOException {
        String json = result.getResponse().getContentAsString();

        return (T) jacksonMapper.readValue(json, clsType);
    }

    public <T> T getEntityResult(ResponseEntity<String> result, Class<T> clsType) throws JsonParseException, JsonMappingException, IOException {
        return getJacksonMapper().readValue(result.getBody(), clsType);
    }

    public <T> T getEntityResult(ResponseEntity<String> result, TypeReference<T> typeRef) throws JsonParseException, JsonMappingException, IOException {
        return getJacksonMapper().readValue(result.getBody(), typeRef);
    }

    public String getEntityJson(Object entity) throws JsonParseException, JsonMappingException, IOException {
        return getJacksonMapper().writeValueAsString(entity);
    }

    public <T> T getEntityResults(ResponseEntity<String> result, TypeRef<T> type) {
        String json = result.getBody();
        if (log.isTraceEnabled()) {
            log.trace("getEntityResults.typeRef: {} -> {}", type.getType(), resourceJsonPath);
        }
        return JsonPath.using(JSONPATH_CONF).parse(json).read(this.resourceJsonPath, type);
    }

    public <T> T getEntityResults(MvcResult result, TypeRef<T> typeRef) throws UnsupportedEncodingException {
        String json = result.getResponse().getContentAsString();

        return JsonPath.using(JSONPATH_CONF).parse(json).read(this.resourceJsonPath, typeRef);
    }

    public MvcResult performEntityMvcGet() throws Exception {
        return performEntityMvcGet(null, null);
    }

    public MvcResult performEntityMvcGet(String uri) throws Exception {
        return performEntityMvcGet(uri, null);
    }

    public MvcResult performEntityMvcGet(String uriPath, MultiValueMap<String, String> params) throws Exception {
        URI uri = getUriFromBaseParams(uriPath, params);
        MvcResult result = this.mockMvc.perform(get(uri)
                .with(user(DEFAULT_USER).password(DEFAULT_PASS))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print()).andReturn();

        return result;
    }

    public MvcResult performEntityMvcPost(String content) throws Exception {
        URI uri = getUriFromBase(null);
        return this.mockMvc.perform(post(uri)
                .with(user(DEFAULT_USER).password(DEFAULT_PASS))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andDo(print()).andReturn();
    }

    public MvcResult performEntityMvcPut(String uriPath, String content) throws Exception {
        URI uri = getUriFromBase(uriPath);
        return this.mockMvc.perform(put(uri)
                .with(user(DEFAULT_USER).password(DEFAULT_PASS))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andDo(print()).andReturn();
    }

    public MvcResult performEntityMvcPatch(String uriPath, String content) throws Exception {
        URI uri = getUriFromBase(uriPath);
        return this.mockMvc.perform(patch(uri)
                .with(user(DEFAULT_USER).password(DEFAULT_PASS))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andDo(print()).andReturn();
    }

    public MvcResult performEntityMvcDelete(String uriPath) throws Exception {
        URI uri = getUriFromBase(uriPath);
        return this.mockMvc.perform(delete(uri)
                .with(user(DEFAULT_USER).password(DEFAULT_PASS))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print()).andReturn();
    }

    private URI getUriFromBase(String uriPath) {
        String baseUri = getEntityUriPath(uriPath);
        return UriComponentsBuilder.fromUriString(baseUri).build().toUri();
    }

    protected URI getUriFromBaseParams(String uriPath, MultiValueMap<String, String> params) {
        String baseUri = getEntityUriPath(uriPath);

        URI uri = UriComponentsBuilder.fromUriString(baseUri).queryParams(params).build().toUri();

        if (log.isTraceEnabled()) {
            log.trace("URI: {}", uri.toString());
        }
        return uri;
    }

    protected String getEntityUriPath() {
        return getEntityUriPath(null);
    }

    protected String getEntityUriPath(String subPath) {
        return API_PREFIX + (resourceName != null ? "/" + resourceName : "") + (subPath != null ? "/" + subPath : "");
    }

    public ResponseEntity<String> performEntityRestGet() throws JsonParseException, JsonMappingException, IOException {
        return performEntityRestGet(null, null);
    }

    public ResponseEntity<String> performEntityRestGet(String uriSubPath) throws JsonParseException, JsonMappingException, IOException {
        return performEntityRestGet(uriSubPath, null);
    }

    public ResponseEntity<String> performEntityRestPost(String content) throws JsonParseException, JsonMappingException, IOException {
        return performEntityRestPost(null, content);
    }

    public ResponseEntity<String> performEntityRestPost(String uriSubPath, String content) throws JsonParseException, JsonMappingException, IOException {
        String uri = getUriBase() + getEntityUriPath(uriSubPath);

        HttpEntity<String> entity = new HttpEntity<String>(content, getDefaultUpdateHeaders());

        if (log.isTraceEnabled()) {
            log.trace("Post.call: {} -> {}", uri, entity);
        }
        ResponseEntity<String> response = getRestTemplateWithAuth().exchange(uri, HttpMethod.POST, entity, String.class);
        if (log.isTraceEnabled()) {
            log.trace("Post.response: {} -> {}", uri, Util.logResponseEntityToString(response));
        }
        return response;
    }

    public ResponseEntity<String> performEntityRestPut(String uriSubPath, String content) {
        String uri = getUriBase() + getEntityUriPath(uriSubPath);
        HttpEntity<String> entity = new HttpEntity<String>(content, getDefaultUpdateHeaders());
        if (log.isTraceEnabled()) {
            log.trace("Put.call: {} -> {}", uri, entity);
        }
        ResponseEntity<String> response = getRestTemplateWithAuth().exchange(uri, HttpMethod.PUT, entity, String.class);
        if (log.isTraceEnabled()) {
            log.trace("Put.response: {} -> {}", uri, Util.logResponseEntityToString(response));
        }
        return response;
    }

    public ResponseEntity<String> performRestGet(String uri) {
        if (log.isTraceEnabled()) {
            log.trace("Get.call: {}", uri);
        }
        ResponseEntity<String> response = getRestTemplateWithAuth().getForEntity(uri, String.class);
        if (log.isTraceEnabled()) {
            log.trace("Get.response: {} -> {}", response.getStatusCodeValue(), response.getBody());
        }
        return response;
    }

    public ResponseEntity<String> performRestDelete(String uri) {
        if (log.isTraceEnabled()) {
            log.trace("Delete.call: {}", uri);
        }
        ResponseEntity<String> response = this.getRestTemplateWithAuth().exchange(uri, HttpMethod.DELETE, null, String.class);

        if (log.isTraceEnabled()) {
            log.trace("Delete.response: {} -> {}", response.getStatusCodeValue(), response.getBody());
        }
        return response;
    }

    public ResponseEntity<String> performEntityRestDelete(String uriSubPath) {
        URI uri = getUriFromBaseParams(uriSubPath, null);
        return performRestDelete(uri.toString());
    }

    public ResponseEntity<String> performEntityRestGet(String uriSubPath, MultiValueMap<String, String> params) throws JsonParseException, JsonMappingException, IOException {
        URI uri = getUriFromBaseParams(uriSubPath, params);
        return performRestGet(uri.toString());
    }

    public static String getRecordIdFromResult(MvcResult mvcResult) throws URISyntaxException, UnsupportedEncodingException {
        String selfUri = getResponseSelfRef(mvcResult.getResponse().getContentAsString());
        return getUriPathEnd(selfUri);
    }

    public static String getUriPathEnd(String selfUri) throws URISyntaxException {
        return new File(new URI(selfUri).getPath()).getName();
    }

    public static String getRecordIdFromResult(ResponseEntity<String> restResult) throws URISyntaxException {
        String selfUri = getResponseSelfRef(restResult.getBody());
        return getUriPathEnd(selfUri);
    }

    public static String getResponseSelfRef(String body) {
        return JsonPath.parse(body, JSONPATH_CONF)
                .read("$._links.self.href", String.class);
    }

    protected void performTestDataRestPost(List<RecordJson<? extends Record>> records) throws Exception {
        for (RecordJson<? extends Record> record : records) {
            performEntityRestPost(record.getRecordJson());
        }
    }

    public MultiValueMap<String, String> getDefaultUpdateHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE); // Content-Type: application/json;charset=UTF-8

        return headers;
    }

    protected TestRestTemplate getRestTemplateWithAuth() {
        return testRestTemplate.withBasicAuth(DEFAULT_USER, DEFAULT_PASS);
    }

    public static String getApiEntityName(Class<?> entity) {
        return entity.getSimpleName().toLowerCase();
    }

    public static String getApiResourceName(Class<?> entity) {
        return getApiEntityName(entity) + "s"; // plural nouns for API resources 
    }

    public <T> void processRestEntityGetPages(ResponseEntity<String> response,
                                              Map<Long, RecordJson<? extends Record>> recordIdsIdx,
                                              ListTypeRef<? extends Record> listTypeRef)
            throws Exception {

        final String entityBasePath = getUriBase() + getEntityUriPath(null);

        ApiPageHandlerCallback pageHandler = new ApiPageHandlerCallback() {

            @Override
            public String processPage(ResponseEntity<String> response) throws Exception {
                if (log.isTraceEnabled()) {
                    log.trace("ResponseBody.json: {}", response.getBody());
                }

                List<? extends Record> pageRecords = getEntityResults(response, listTypeRef);

                if (log.isTraceEnabled()) {
                    log.trace("ResponseBody.records: {}", pageRecords.size());
                }
                for (Record recordReceived : pageRecords) {
                    Long recordReceivedId = getRecordId(recordReceived);

                    if (recordIdsIdx.containsKey(recordReceivedId)) {
                        RecordJson<? extends Record> recordSent = recordIdsIdx.get(recordReceivedId);
                        assertThat(recordReceived.equalsData(recordSent.getRecord()), equalTo(true)); // check that the record data we're receiving equals the data we sent in
                        recordSent.setSeen(true); // set a flag to indicate this record has been seen in the API output
                    }
                }

                HalPage resultPageInfo = getEntityResult(response, HalResponse.class).getPage();
                Integer totalPages = resultPageInfo.getTotalPages();
                Integer currentPageIdx = resultPageInfo.getNumber(); // get current page *index*

                if (log.isTraceEnabled()) {
                    log.trace("ResponseBody.idx/pages: {}", currentPageIdx, totalPages);
                }

                String expectedFirstExpr = Pattern.quote(entityBasePath + "?page=0&size=" + DEFAULT_PAGE_SIZE) + ".*";
                String first = getHalLinkFromResponse(response, Link.REL_FIRST);
                assertThat(first, matchesPattern(expectedFirstExpr));

                String nextUrl = null;
                if (currentPageIdx + 1 < totalPages) { // don't validate next URI if it's the last page (the field doesn't exist) 
                    String expectedNextUrlExpr = Pattern.quote(entityBasePath + "?page=" + (currentPageIdx + 1) + "&size=" + DEFAULT_PAGE_SIZE) + ".*";

                    nextUrl = getHalLinkFromResponse(response, Link.REL_NEXT);
                    assertThat(nextUrl, matchesPattern(expectedNextUrlExpr));

                    if (log.isTraceEnabled()) {
                        log.trace("ResponseBody.nextUrl: {}", nextUrl);
                    }
                }

                String expectedLastExpr = Pattern.quote(entityBasePath + "?page=") + "\\d+&size=" + DEFAULT_PAGE_SIZE + ".*";
                String last = getHalLinkFromResponse(response, Link.REL_LAST);
                assertThat(last, matchesPattern(expectedLastExpr));

                return nextUrl;
            }
        };

        String nextUri = pageHandler.processPage(response);
        if (nextUri != null) {
            ResponseEntity<String> nextResponse = performRestGet(nextUri);
            processRestEntityGetPages(nextResponse, recordIdsIdx, listTypeRef); // recurse through pages
        }
    }

    public static String getHalLinkFromResponse(ResponseEntity<String> response, String linkName) {
        return JsonPath.parse(response.getBody(), JSONPATH_CONF).read("$._links." + linkName + ".href", String.class);
    }

    /**
     * Get the Id from the links URI contained in a record
     */
    public Long getRecordId(Record record) throws Exception {
        try {
            String recordId = "" + JXPathContext.newContext(record.get_links()).getValue("self/href");
            return Long.parseLong(getUriPathEnd(recordId));
        } catch (JXPathNotFoundException jxe) {
            throw new Exception("Object was: " + record, jxe);
        }
    }

    public void validateEntityLinksForRecord(ResponseEntity<String> response, String recordId) {

        // Check: _links -> self -> href = e.g: http://localhost:8080/api/employees/5
        String linkSelf = JsonPath.parse(response.getBody(), JSONPATH_CONF).read("$._links.self.href", String.class);

        String expectedLinkSelf = getUriBase() + getEntityUriPath() + "/" + recordId;
        assertThat(linkSelf, equalTo(expectedLinkSelf));

        // Check: _links -> entity -> href = e.g: http://localhost:8080/api/employees/5
        String linkEntity = JsonPath.parse(response.getBody(), JSONPATH_CONF).read("$._links." + getEntityName() + ".href", String.class);

        assertThat(linkEntity, equalTo(expectedLinkSelf));
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<RecordJson<? extends Record>> getRecordsFromCsvUsingLoader(String csvResourcePath, int expectedRecordCount, Class<? extends Record> recordType) throws IOException {

        // preprocess CSV data in to POJOs and JSON (single thread)
        List<RecordJson<? extends Record>> records = new ArrayList<>();

        CrudRepository crudRepo = Mockito.mock(CrudRepository.class);

        when(crudRepo.save(any(recordType.getClass()))).thenAnswer(new Answer<Record>() {
            @Override
            public Record answer(InvocationOnMock invocation) throws Throwable {
                Record record = invocation.getArgumentAt(0, Record.class);
                String recordJson = getEntityJson(record);
                records.add(new RecordJson(record, recordJson));
                return record;
            }
        });

        try (InputStreamReader inputReader = new InputStreamReader(ResourceTestSupport.class.getResourceAsStream(csvResourcePath))) {
            DataLoader.loadEmployees(crudRepo, inputReader);
        }

        assertThat(records.size(), equalTo(expectedRecordCount));

        return records;
    }

    /**
     * POST a number of records to a resource URI, concurrently in batches. This:
     * - splits the records we need to send up in to batches
     * - sends them in parallel to the API
     * - validates the data output from each call
     * - gets the ID from each response and update local records with it for future reference
     * 
     * @param sendThreads number of send threads to use
     * @param newRecords list of records to split up and send across the threads
     * @param newRecordsIdx a map to populate with the record id and sent record, after creating the record
     * @param recordType the type of record
     * @param maxTimeTaken validate the maximum time that should be taken for this call
     */
    public <T extends Record> void postRecordsInParallel(int sendThreads, List<RecordJson<? extends Record>> newRecords,
                                                         Map<Long, RecordJson<? extends Record>> newRecordsIdx, Class<T> recordType, long maxTimeTaken) {

        log.info("POST.start: {}: {} - {} threads", recordType.getSimpleName(), newRecords.size(), sendThreads);

        int batchSize = newRecords.size() / sendThreads;
        final List<Exception> apiErrors = Collections.synchronizedList(new ArrayList<Exception>());

        long dtBefore = new Date().getTime();

        IntStream.range(0, (newRecords.size() + batchSize - 1) / batchSize)
                .mapToObj(i -> newRecords.subList(i * batchSize, Math.min(newRecords.size(), (i + 1) * batchSize)))
                .parallel()
                .forEach(batch -> {
                    for (RecordJson<? extends Record> record : batch) {
                        try {
                            ResponseEntity<String> response = performEntityRestPost(record.getRecordJson());

                            assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

                            T responseRecord = getEntityResult(response, recordType);

                            assertThat(responseRecord.equalsData(record.getRecord()), equalTo(true)); // check the record returned is equal to what was sent in
                            long newRecordId = Long.parseLong(getRecordIdFromResult(response));

                            if (log.isTraceEnabled()) {
                                log.trace("{} -> {}", newRecordId, record.getRecordJson());
                            }

                            record.getRecord().setId(newRecordId); // set the returned DB ID in our local record, used to validate the record in a GET later
                            newRecordsIdx.put(newRecordId, record); // create add record to an index so we can look it up later
                        } catch (Exception e) {
                            log.error("Error: {}", ExceptionUtils.getStackTrace(e));
                            apiErrors.add(e);
                            break;
                        }
                    }
                    log.info("PostRecordsInParallel.batch.completed[{}]: {}", recordType.getSimpleName(), batchSize);
                });
        long dtAfter = new Date().getTime();

        assertThat(apiErrors.size(), equalTo(0));

        long loadTimeTaken = dtAfter - dtBefore;
        if (log.isTraceEnabled()) {
            log.trace("POST.end.TimeMs: {}[{}]: {} - {} threads", recordType.getSimpleName(), newRecords.size(), loadTimeTaken, sendThreads);
        }

        assertThat(loadTimeTaken, lessThan(maxTimeTaken));
    }
}
