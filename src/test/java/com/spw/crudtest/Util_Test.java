package com.spw.crudtest;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;

public class Util_Test {
    private static final Logger log = LoggerFactory.getLogger(Util_Test.class);

    @Test
    public void test_getVersion() {
        BuildProperties props = Mockito.mock(BuildProperties.class);
        when(props.getArtifact()).thenReturn("myArtifact");
        when(props.getVersion()).thenReturn("1.0.0-SNAPSHOT");

        String ver = Util.getVersion(props);

        assertThat(ver, equalTo("myArtifact - 1.0.0-SNAPSHOT"));
    }

    @Test
    public void test_httpHeadersToString() {
        Map<String, String> testHeaders = new LinkedHashMap<String, String>();
        testHeaders.put("key1", "val1");
        testHeaders.put("key2", "val2");
        testHeaders.put("key3", "val3");

        HttpHeaders headers = Mockito.mock(HttpHeaders.class);
        when(headers.toSingleValueMap()).thenReturn(testHeaders);

        String headersStr = Util.httpHeadersToString(headers);

        assertThat(headersStr, equalTo("{key1=val1, key2=val2, key3=val3}"));
    }

    @Test
    public void test_httpServletRequestHeadersToString() {
        Map<String, String> testHeaders = new LinkedHashMap<String, String>();
        testHeaders.put("key1", "val1");
        testHeaders.put("key2", "val2");
        testHeaders.put("key3", "val3");

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getHeaderNames()).thenReturn(Collections.enumeration(testHeaders.keySet()));
        when(request.getHeader(any(String.class))).thenAnswer(key -> {
            return testHeaders.get(key.getArgumentAt(0, String.class));
        });
        
        String headersStr = Util.httpServletRequestHeadersToString(request);

        assertThat(headersStr, equalTo("{key1=val1, key2=val2, key3=val3}"));
        
        Logger logger = Mockito.mock(Logger.class);
        when(logger.isTraceEnabled()).thenReturn(true);
        Util.logRequest(logger, request); // just test that it doesnt throw any exceptions
    }
    
    @Test
    public void test_logRequestHttp() throws URISyntaxException {
       
        HttpHeaders headers = new HttpHeaders();
        headers.add("key1", "val1");
        headers.add("key2", "val2");
        headers.add("key3", "val3");

        HttpRequest request = Mockito.mock(HttpRequest.class);
        when(request.getHeaders()).thenReturn(headers);
        when(request.getMethod()).thenReturn(HttpMethod.GET);
        when(request.getURI()).thenReturn(new URI("http://localhost"));

        Logger logger = Mockito.mock(Logger.class);
        when(logger.isTraceEnabled()).thenReturn(true);
        Util.logRequest(logger, request);
    }
    
    @Test
    public void test_responseEntityToString() {
       
        String jsonIn = "{ \"key\" : \"val\" }";

        @SuppressWarnings("unchecked")
        ResponseEntity<String> entity = (ResponseEntity<String>) Mockito.mock(ResponseEntity.class);
        
        when(entity.getBody()).thenReturn(jsonIn);
       
        String output = Util.logResponseEntityToString(entity);

        assertThat(output, equalTo("0: '" + jsonIn + "'"));
    }
}
