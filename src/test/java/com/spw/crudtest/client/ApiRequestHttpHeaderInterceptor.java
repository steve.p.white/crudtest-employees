package com.spw.crudtest.client;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * Intercept HTTP requests and set a header on the call
 */
public class ApiRequestHttpHeaderInterceptor implements ClientHttpRequestInterceptor {
    
    private final String name;
    private final String value;

    public ApiRequestHttpHeaderInterceptor(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {        
        request.getHeaders().set(name, value); // e.g set Accept: text/html
        return execution.execute(request, body);
    }
}
