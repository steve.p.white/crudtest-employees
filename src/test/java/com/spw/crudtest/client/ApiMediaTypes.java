package com.spw.crudtest.client;

import com.google.common.base.Charsets;
import org.springframework.http.MediaType;

public class ApiMediaTypes {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                                        MediaType.APPLICATION_JSON.getSubtype(),
                                                                        Charsets.UTF_8);
    public static final MediaType TEXT_HTML_UTF8 = new MediaType(MediaType.TEXT_HTML.getType(),
                                                                 MediaType.TEXT_HTML.getSubtype(),
                                                                 Charsets.UTF_8);
}
