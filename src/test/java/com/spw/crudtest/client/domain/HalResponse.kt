package com.spw.crudtest.client.domain

class HalResponse {
	var _embedded: Map<String, Any>? = null;
	var _links: Map<String, Any>? = null
	var page: HalPage? = null;
}