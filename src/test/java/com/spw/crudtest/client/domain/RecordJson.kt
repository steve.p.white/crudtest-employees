package com.spw.crudtest.client.domain

class RecordJson<T> constructor (record : T, recordJson : String) {
	var recordJson : String? = recordJson;
    var record : T? = record;
    var seen : Boolean? = null;
}