package com.spw.crudtest.client.domain

class HalPage {
	var size: Int? = null
	var totalElements: Long? = null
	var totalPages: Int? = null
	var number: Int? = null
}