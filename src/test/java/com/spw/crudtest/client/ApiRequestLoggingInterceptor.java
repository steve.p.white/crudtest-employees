package com.spw.crudtest.client;

import java.io.IOException;

import com.spw.crudtest.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * Intercept HTTP requests log the call and it's response
 */
public class ApiRequestLoggingInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger log = LoggerFactory.getLogger(ApiRequestLoggingInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        Util.logRequest(log, request);
        ClientHttpResponse response = execution.execute(request, body);

        if (log.isTraceEnabled()) {
            String headers = Util.httpHeadersToString(response.getHeaders());
            log.trace("Response[{}]: {}: {}, headers: [{}],", request.hashCode(), response.getRawStatusCode(), response.getStatusText(), headers);
        }
        return response;
    }
}
