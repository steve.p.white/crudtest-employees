package com.spw.crudtest.client;

import org.apache.commons.lang3.StringUtils;

public class ApiParams {

    public static final String PAGE = "page";
    public static final String SIZE = "size";
    public static final String SORT = "sort";

    public static String[] getSupportedParams() {
        return new String[] { PAGE, SIZE, SORT };
    }
    
    public static String getSupportedParamsValue() {
        return StringUtils.join(getSupportedParams(), ",");
    }
}
