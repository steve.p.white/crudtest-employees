package com.spw.crudtest;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.jayway.jsonpath.TypeRef;

/**
 * Return a JsonPath TypeRef for a parameterised list of a particular type
 */
public class ListTypeRef<T> extends TypeRef<List<T>> {

    protected final Type type;

    protected ListTypeRef() {
        Type superClass = getClass().getGenericSuperclass();
        if (superClass instanceof Class<?>) {
            throw new IllegalArgumentException("No type info in TypeRef");
        }
        Type genericType = ((ParameterizedType) superClass).getActualTypeArguments()[0];
        TypeToken<?> typeToken = TypeToken.of(genericType);

        this.type = getParameterisedList(typeToken);
    }

    @SuppressWarnings("serial")
    private static <T> Type getParameterisedList(TypeToken<T> type) {
        return new TypeToken<List<T>>() {
        }.where(new TypeParameter<T>() {
        }, type).getType();
    }

    public Type getType() {
        return type;
    }
}
