package com.spw.crudtest.domain;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class Employee_Test {
    
    @Test
    public void test_compareObjects() {
        // test pojo hashcode/equals

        Employee emp1 = new Employee("Joe", "Bloggs");
        Employee emp2 = new Employee("Jimmy", "White");
        Employee emp3 = new Employee("Joe", "Bloggs");
        
        assertThat(emp1.equalsRecord(null), equalTo(false));
        assertThat(emp1.equalsRecord(""), equalTo(false));
        assertThat(emp1.equalsRecord(emp1), equalTo(true));
        
        assertThat(emp1.equalsData(null), equalTo(false));
        assertThat(emp1.equalsData(""), equalTo(false));
        assertThat(emp1.equalsData(emp1), equalTo(true));
          
        assertThat(emp1.equalsData(emp3), equalTo(true));
       
        assertThat(emp1.equalsData(emp2), equalTo(false));
        
        emp1.setId(1L);
        emp3.setId(2L);
        
        assertThat(emp1.equalsRecord(emp3), equalTo(false));
    }
}
