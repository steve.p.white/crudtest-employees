package com.spw.crudtest.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import java.util.Map;

import com.spw.crudtest.ResourceTestSupport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@SuppressWarnings("unchecked")
public class Metrics_Test extends ResourceTestSupport {
    private static final Logger log = LoggerFactory.getLogger(Metrics_Test.class);
    
    public Metrics_Test() {
        super(null);
    }
        
    @Test
    public void test_getMetrics() throws Exception { 
        // version endpoint is not authenticated
        ResponseEntity<String> response = getRestTemplateWithAuth().getForEntity("/metrics", String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        
        Map<String,Integer> metrics = getEntityResult(response, Map.class);
        if (log.isTraceEnabled()) {
            log.trace("metrics: {}", metrics);
        }
        
        assertThat(metrics.get("mem"), greaterThan(0));
        assertThat(metrics.get("processors"), greaterThan(0));
        assertThat(metrics.get("classes.loaded"), greaterThan(0));
        assertThat(metrics.get("counter.status.200.health"), greaterThan(0));
    }
    
    @Test
    public void test_getHealth() throws Exception { 
        ResponseEntity<String> response = getRestTemplateWithAuth().getForEntity("/health", String.class);
       
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        
        Map<String,Object> values = getEntityResult(response, Map.class);
        assertThat("" + values.get("status"),equalTo("UP"));
    }
}
