package com.spw.crudtest.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.spw.crudtest.ResourceTestSupport;
import com.spw.crudtest.client.ApiMediaTypes;
import com.spw.crudtest.client.ApiRequestHttpHeaderInterceptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class Meta_Test extends ResourceTestSupport {
    public Meta_Test() {
        super(null);
    }

    /**
     * Simple test to check the Spring context loads OK
     */
    @Test
    public void test_contextLoads() throws Exception {
    }


    @Test
    public void test_get404_notFound() throws Exception {

        Map<String, Object> errResponse = getEntityResult(super.performRestGet("/api/invalidPath"), TYPEREF_MAP_STRING_OBJECT);

        assertThat("timestamp", (Long) errResponse.get("timestamp"), greaterThan(timeBefore));

        assertThat((int) errResponse.get("status"), equalTo(HttpStatus.NOT_FOUND.value()));
        assertThat("" + errResponse.get("error"), equalTo(HttpStatus.NOT_FOUND.getReasonPhrase()));
        assertThat("" + errResponse.get("message"), equalTo(HttpStatus.NOT_FOUND.getReasonPhrase()));
        assertThat("" + errResponse.get("path"), equalTo("/api/invalidPath"));
    }
    
    @Test
    @SuppressWarnings("unchecked")
    public void test_getBuildInfo() throws Exception {
        // version endpoint is not authenticated
        ResponseEntity<String> response = super.restTemplate.getForEntity("/info", String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getHeaders().getContentType(), equalTo(MediaType.APPLICATION_JSON_UTF8));

        Map<String, Map<String, Object>> values = getEntityResult(response, Map.class);

        Map<String, Object> buildValues = values.get("build");

        assertTrue(("" + buildValues.get("version")).matches("\\d+\\.\\d+\\.\\d+.*"));

        assertNotNull(buildValues.get("artifact"));
        assertNotNull(buildValues.get("name"));
        assertNotNull(buildValues.get("group"));
        assertNotNull(buildValues.get("time"));
    }

    /**
     * Get a TestRestTemplate with an interceptor which sets Accept: text/html, add the other interceptors after (e.g logging).
     */
    public TestRestTemplate getRestTemplateWithAuthHtml() {

        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(new ApiRequestHttpHeaderInterceptor(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE));
        interceptors.addAll(getStandardApiInterceptors());

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(interceptors);

        return new TestRestTemplate(restTemplate);
    }

    private ResponseEntity<String> performRestGetWithAuthHtml(String uriPath, MultiValueMap<String, String> uriParams) {

        String uriFull = getUriBase() + "/" + uriPath;

        URI uri = UriComponentsBuilder.fromUriString(uriFull).queryParams(uriParams).build().toUri();
        return getRestTemplateWithAuthHtml().getForEntity(uri, String.class);
    }

    @Test
    public void test_getSwaggerUi() throws Exception {
        ResponseEntity<String> response = performRestGetWithAuthHtml("/swagger-ui.html", null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getHeaders().getContentType(), equalTo(ApiMediaTypes.TEXT_HTML_UTF8));
    }
}
