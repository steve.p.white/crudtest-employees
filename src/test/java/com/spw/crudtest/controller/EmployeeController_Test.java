package com.spw.crudtest.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.validation.ConstraintViolationException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.jayway.jsonpath.JsonPath;
import com.spw.crudtest.ListTypeRef;
import com.spw.crudtest.ResourceTestSupport;
import com.spw.crudtest.client.ApiParams;
import com.spw.crudtest.client.HalConstants;
import com.spw.crudtest.client.domain.HalPage;
import com.spw.crudtest.client.domain.HalResponse;
import com.spw.crudtest.client.domain.RecordJson;
import com.spw.crudtest.domain.Employee;
import com.spw.crudtest.domain.Record;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.NestedServletException;

/**
 * Test the CRUD API for the Employee domain class
 * Each test performs a call to Via a REST call (via Jetty) and validates the output
 * Some tests also perform the same call to the MVC framework directly (no web container)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@AutoConfigureMockMvc
public class EmployeeController_Test extends ResourceTestSupport {

    private static final Logger log = LoggerFactory.getLogger(EmployeeController_Test.class);

    private static final ListTypeRef<Employee> JSONPATH_TYPEREF_LIST = new ListTypeRef<Employee>() {
    };

    private static final TypeReference<List<Employee>> JACKSON_TYPEREF_LIST = new TypeReference<List<Employee>>() {
    };

    public EmployeeController_Test() {
        super(Employee.class); // set the entity under test
    }

    @Test
    public void test_testBasicAuthEnforced() {
        validateBasicAuthOnEntityRestApi();
    }

    @Test
    public void test_postBadNonEntityData_throwsError() throws JsonParseException, JsonMappingException, IOException, Exception {
        String invalidData = "invalidData...";
        MvcResult result = performEntityMvcPost(invalidData); // try bad POST via MVC call
        assertThat(result.getResponse().getStatus(), equalTo(HttpStatus.BAD_REQUEST.value())); // fail due to non-JSON data

        ResponseEntity<String> resultRest = performEntityRestPost(invalidData); // try bad POST via REST call
        assertThat(resultRest.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        invalidData = "{ \"key\" : \"invalidData...\" }"; // try bad JSON POST via MVC
        try {
            result = performEntityMvcPost(invalidData);
            fail();
        } catch (NestedServletException nse) {
            log.error("CaughtException: {}" + ExceptionUtils.getMessage(nse));
            assertThat(nse.getCause(), instanceOf(DataIntegrityViolationException.class));
        }

        resultRest = performEntityRestPost(invalidData); // try bad JSON POST via REST
        assertThat(resultRest.getStatusCode(), equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * Get all employees (a single page worth). This will always be a minimum of 5 records regardless of test order
     */
    @Test
    public void test_getAllEmployees() throws Exception {

        List<Employee> employeeResultsMvc = getEntityResults(performEntityMvcGet(), JSONPATH_TYPEREF_LIST);
        assertThat(employeeResultsMvc.size(), greaterThanOrEqualTo(5));

        List<Employee> employeeResultsRest = getEntityResults(performEntityRestGet(), JSONPATH_TYPEREF_LIST);
        assertThat(employeeResultsRest.size(), greaterThanOrEqualTo(5));
    }

    @Test
    public void test_getFirstEmployee() throws Exception {
        List<Employee> employeeRecs = new ArrayList<Employee>();

        employeeRecs.add(getEntityResult(performEntityMvcGet("/1"), Employee.class)); // get via MVC
        employeeRecs.add(getEntityResult(performEntityRestGet("/1"), Employee.class)); // get via REST

        employeeRecs.forEach(employee -> {
            assertThat(employee.getTimeUpdated(), greaterThan(timeBefore));
            assertThat(employee.getFirstName(), equalTo("Jeff"));
            assertThat(employee.getLastName(), equalTo("Stelling"));
        });
    }

    @Test
    public void test_getSecondEmployee() throws Exception {
        List<Employee> employeeRecs = new ArrayList<Employee>();

        employeeRecs.add(getEntityResult(performEntityMvcGet("/2"), Employee.class));
        employeeRecs.add(getEntityResult(performEntityRestGet("/2"), Employee.class));

        employeeRecs.forEach(employee -> {
            assertThat(employee.getTimeUpdated(), greaterThan(timeBefore));
            assertThat(employee.getFirstName(), equalTo("Chris"));
            assertThat(employee.getLastName(), equalTo("Kamara"));
        });
    }

    @Test
    public void test_getInvalidEmployee() throws Exception {
        MvcResult result = performEntityMvcGet("/9999999999");
        assertThat(result.getResponse().getStatus(), equalTo(HttpStatus.NOT_FOUND.value()));

        ResponseEntity<String> resultRest = performEntityRestGet("/9999999999");
        assertThat(resultRest.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    /**
     * Invoked by other test methods to post an employee record via MVC & REST methods. Each:
     * - Validate returned record is correct
     * - Explicitly get the record and validate it's correct
     * - Validate the timeAdded/timeUpdated fields
     */
    public String postEmployeeTestsOk(Employee employee) throws Exception {
        Long timeAdded = employee.getTimeAdded();
        Long timeUpdated = employee.getTimeUpdated();
        String employeeJson = getEntityJson(employee);

        // MVC test
        MvcResult mvcResult = performEntityMvcPost(employeeJson);
        assertThat(mvcResult.getResponse().getStatus(), equalTo(HttpStatus.CREATED.value()));

        Employee employeeOut = getEntityResult(mvcResult, Employee.class);
        assertThat(employeeOut.equalsData(employee), equalTo(true)); // ensure the data sent back from the post reflects the data sent in
        String employeeOutMvcId = getRecordIdFromResult(mvcResult);

        Employee employeeOutGet = getEntityResult(performEntityMvcGet("/" + employeeOutMvcId), Employee.class); // read the record again from the API
        assertThat(employeeOutGet.equalsData(employee), equalTo(true));
        assertThat(employeeOutGet.getTimeAdded(), not(equalTo(timeAdded))); // any timeAdded/Updated values sent in should be overridden by the API 
        assertThat(employeeOutGet.getTimeUpdated(), not(equalTo(timeUpdated)));

        // REST test
        ResponseEntity<String> restResult = performEntityRestPost(employeeJson);
        assertThat(restResult.getStatusCode(), equalTo(HttpStatus.CREATED));

        employeeOut = getEntityResult(restResult, Employee.class); // read the record again from the API
        assertThat(employeeOut.equalsData(employee), equalTo(true)); // ensure the data sent back from the post reflects the data sent in
        String employeeOutRestId = getRecordIdFromResult(restResult);

        employeeOutGet = getEntityResult(performEntityRestGet("/" + employeeOutRestId), Employee.class);
        assertThat(employeeOutGet.equalsData(employee), equalTo(true)); // ensure that the returned data is equal to what was sent in

        assertThat(employeeOutRestId, not(equalTo(employeeOutMvcId))); // check that the record id has changed for the same input data as the MVC test
        assertThat(employeeOutGet.getTimeAdded(), not(equalTo(timeAdded))); // any timeAdded/Updated values sent in should be overridden by the API 
        assertThat(employeeOutGet.getTimeUpdated(), not(equalTo(timeUpdated)));
        
        return employeeOutRestId;
    }

    /**
     * Create an employee - simple test
     */
    @Test
    public void test_postEmployee() throws JsonParseException, JsonMappingException, IOException, Exception {
        Employee employee = new Employee("Joe", "Bloggs");
        employee.setNewRecord(); // explicitly set time fields, these should be overridden by the API
        postEmployeeTestsOk(employee);
    }

    /**
     * Post a record containing unicode characters
     */
    @Test
    public void test_postEmployeeUnicode() throws JsonParseException, JsonMappingException, IOException, Exception {
        Employee employee = new Employee("JoƐ", "BloggṠ");
        employee.setNewRecord();
        postEmployeeTestsOk(employee);
    }

    @Test
    public void test_postEmployee_invalidData_long() throws JsonParseException, JsonMappingException, IOException, Exception {
        // max field lengths for firstName/lastName = 32
        Employee employee = new Employee("JoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoe", "BloggsBloggsBloggsBloggsBloggsBloggs");
        postEmployeeTestsInvalid(employee);
    }

    @Test
    public void test_postEmployee_invalidData_short() throws JsonParseException, JsonMappingException, IOException, Exception {
        // min field lengths for firstName/lastName = 1
        Employee employee = new Employee("", "");
        postEmployeeTestsInvalid(employee);
    }

    @Test
    public void test_putBadEmployee() throws JsonParseException, JsonMappingException, IOException {
        Employee employee = new Employee("Joe", "BloggsDoesntAlreadyExist");
        ResponseEntity<String> result = performEntityRestPut("/8888888", getEntityJson(employee));
        assertThat(result.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    @Test
    public void test_putEmployee() throws Exception {
        Employee employee = new Employee("Joe", "Bloggs");

        // add a record and get it's ID from the response
        ResponseEntity<String> result = performEntityRestPost(getEntityJson(employee));
        String employeeId = getRecordIdFromResult(result);

        // ensure record is there and the data is the same as what we sent in
        ResponseEntity<String> resultGet = performEntityRestGet("/" + employeeId);
        assertThat(resultGet.getStatusCode(), equalTo(HttpStatus.OK));

        Employee employeeGet = getEntityResult(resultGet, Employee.class);
        assertThat(employeeGet.equalsData(employee), equalTo(true));

        Long timeLastUpdated = employeeGet.getTimeUpdated();

        // update the same record
        Employee employeeNew = new Employee("Joe", "BloggsAmendSurname1");

        result = performEntityRestPut("/" + employeeId, getEntityJson(employeeNew));
        assertThat(result.getStatusCode(), equalTo(HttpStatus.CREATED));

        String employeeNewId = getRecordIdFromResult(result);

        assertThat(employeeNewId, equalTo(employeeId)); // check the id in the response and ensure it hasn't created a new record

        // ensure the record has been updated and the timeUpdated field has increased
        Employee employeeNewGet = getEntityResult(result, Employee.class);
        assertThat(employeeNewGet.equalsData(employeeNew), equalTo(true));

        assertThat(employeeNewGet.getTimeUpdated(), greaterThan(timeLastUpdated));

        // update the record again with different firstName/lastName
        timeLastUpdated = employeeGet.getTimeUpdated();
        employeeNew = new Employee("JoeNewName", "BloggsAmendSurname2");

        result = performEntityRestPut("/" + employeeId, getEntityJson(employeeNew));
        assertThat(result.getStatusCode(), equalTo(HttpStatus.CREATED));

        employeeNewId = getRecordIdFromResult(result);
        assertThat(employeeNewId, equalTo(employeeId)); // check the id hasn't changed

        employeeNewGet = getEntityResult(result, Employee.class);
        assertThat(employeeNewGet.equalsData(employeeNew), equalTo(true)); // ensure the record has been updated and the timeUpdated field has increased

        assertThat(employeeNewGet.getTimeUpdated(), greaterThan(timeLastUpdated));

        // update the same record with invalid data - lastName empty
        employeeNew = new Employee("Joe", "");

        result = performEntityRestPut("/" + employeeId, getEntityJson(employeeNew));
        validateRestConstraintError(result);

        // update the same record with invalid data - firstName too long
        employeeNew = new Employee("JoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoe", "");

        result = performEntityRestPut("/" + employeeId, getEntityJson(employeeNew));
        validateRestConstraintError(result);
    }

    @Test
    public void test_getEmployeesSize() throws Exception {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add(ApiParams.SIZE, "1");

        ResponseEntity<String> response = performEntityRestGet(null, params);
        List<? extends Record> results = getEntityResults(response, JSONPATH_TYPEREF_LIST);
        assertThat(results.size(), equalTo(1));

        params = new LinkedMultiValueMap<String, String>();
        params.add(ApiParams.SIZE, "3");

        response = performEntityRestGet(null, params);
        results = getEntityResults(response, JSONPATH_TYPEREF_LIST);

        assertThat(results.size(), equalTo(3));
    }

    @Test
    public void test_getEmployeesPaging() throws Exception {
        performTestDataRestPost(getRecordsFromCsvUsingLoader("/testData/employees-1k.csv", 1000, Employee.class));

        ResponseEntity<String> response = performEntityRestGet();

        HalResponse halResponse = getEntityResult(response, HalResponse.class);

        HalPage page = halResponse.getPage();

        assertThat(page.getSize(), equalTo(20));
        assertThat(page.getTotalElements(), greaterThanOrEqualTo(1000L));
        assertThat(page.getTotalPages(), greaterThan(1));
        assertThat(page.getNumber(), equalTo(0));

        // convert _embedded -> ...
        List<Employee> employees = getJacksonMapper().convertValue(halResponse.get_embedded().get(getResourceName()), JACKSON_TYPEREF_LIST);

        assertThat(employees.size(), equalTo(20)); // validate page size
    }

    @Test
    public void test_validateHalLinks() throws Exception {
        ResponseEntity<String> response = performEntityRestGet();

        // Check _embedded -> _links -> self -> href = http://localhost:8080/api/employees{?page,size,sort}"
        String embeddedSelfHref = JsonPath.parse(response.getBody(), JSONPATH_CONF).read("$._links.self.href", String.class);
        String supportedParams = ApiParams.getSupportedParamsValue();
        String expectedSelfPath = getUriBase() + getEntityUriPath(null) + "{?" + supportedParams + "}";

        assertThat(embeddedSelfHref, equalTo(expectedSelfPath));

        // Check _embedded -> _links -> profile -> href = http://localhost:8080/api/profile/employees
        String embeddedProfileHref = JsonPath.parse(response.getBody(), JSONPATH_CONF).read("$._links.profile.href", String.class);
        String expectedProfileHref = getUriBase() + API_PREFIX + "/" + HalConstants.PROFILE + "/" + getResourceName();

        assertThat(embeddedProfileHref, equalTo(expectedProfileHref));
    }

    @Test
    public void test_validateEntityLinks() throws Exception {
        ResponseEntity<String> response = performEntityRestGet("/5");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        validateEntityLinksForRecord(response, "5");
    }

    /**
     * POST 1k records to the API using a number of send threads.
     * Query all the records page by page (checking page links) and validate all sent records they're there and correct.
     */
    @Test
    public void test_postEmployees1k_parallel() throws Exception {

        int sendThreads = DEFAULT_POST_THREADS;
        List<RecordJson<? extends Record>> employeeRecords = getRecordsFromCsvUsingLoader("/testData/employees-1k.csv", 1000, Employee.class);
        final Map<Long, RecordJson<? extends Record>> employeeRecordsIdx = new ConcurrentHashMap<Long, RecordJson<? extends Record>>();

        // - POST 1k records to the API using a number of send threads. 
        // - Create an index of the returned IDs
        // - Assert that it took less than 60s 
        // Any reasonable build server, even under load, should be able to insert 1k records in under 60s
        postRecordsInParallel(sendThreads, employeeRecords, employeeRecordsIdx, Employee.class, 60000L);

        // Now get all records and validate them...
        ResponseEntity<String> response = performEntityRestGet();

        // - Process API page response validating the data output with data originally sent in
        // - Validate page links
        // - Follow page links recursively

        processRestEntityGetPages(response, employeeRecordsIdx, JSONPATH_TYPEREF_LIST);

        // check that all the records we sent in were seen and validated
        for (RecordJson<? extends Record> record : employeeRecords) {
            assertThat(record.getSeen(), equalTo(true));
        }
    }

    /**
     * Used by tests to post and validate the response for an event known to violate constraints
     * This tests both the MVC and REST API's
     */
    public void postEmployeeTestsInvalid(Employee employee) throws Exception {
        try {
            // MVC should throw a constraint exception
            performEntityMvcPost(getEntityJson(employee));
            fail();
        } catch (NestedServletException nse) {
            log.error("CaughtException: {}" + ExceptionUtils.getMessage(nse));
            assertThat(nse.getCause(), instanceOf(ConstraintViolationException.class));
        }

        // REST should return a JSON error result
        ResponseEntity<String> restResponse = performEntityRestPost(getEntityJson(employee));
        validateRestConstraintError(restResponse);
    }

    
    @Test
    public void test_deleteEmployee() throws Exception {
        Employee employee = new Employee("ToDeleteJoe", "Bloggs");
        String employeeId = postEmployeeTestsOk(employee); // create an employee and get the record Id, before removing them
        
        ResponseEntity<String> deleteResponse = performEntityRestDelete("/" + employeeId);
        assertThat(deleteResponse.getStatusCode(), equalTo(HttpStatus.NO_CONTENT)); // 204

        deleteResponse = performEntityRestDelete("/888888888");
        assertThat(deleteResponse.getStatusCode(), equalTo(HttpStatus.NOT_FOUND)); // 404
        
        ResponseEntity<String> employeeResponse = performEntityRestGet("/" + employeeId); // get the employee and validate that the employee has been deleted
        assertThat(employeeResponse.getStatusCode(),equalTo(HttpStatus.NOT_FOUND));
    }

    //    @Test
    //    public void test_postEmployees1k_parallel_updates() throws Exception {
    //        //TODO - postEmployees1k_parallel_updates
    //    }
    //    

    //    
    //    @Test
    //    public void test_patchEmployee() {
    //        // TODO - test_patchEmployees
    //    }
    
    /**
     * Test that we can delete all records. 
     * Do this at the end of all tests as some rely on the data loaded at startup being present and the order of tests may not always remain the same
     * TODO: Strictly other tests should be independent of one another but time is ticking on...   
     */
    @Test
    public void test_zdeleteAllEmployees() throws Exception {
        ResponseEntity<String> deleteResponse = performEntityRestDelete("/");
        assertThat(deleteResponse.getStatusCode(), equalTo(HttpStatus.NO_CONTENT)); 

        ResponseEntity<String> getResponse = performEntityRestGet();
        HalResponse halResponse = getEntityResult(getResponse, HalResponse.class);
        assertThat(halResponse.getPage().getTotalElements(), equalTo(0L));
    }
}
