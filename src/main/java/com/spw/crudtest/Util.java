package com.spw.crudtest;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;

public class Util {
    public static String getVersion(BuildProperties buildProperties) {
        return buildProperties.getArtifact() + " - " + buildProperties.getVersion();
    }

    public static void logRequest(Logger log, HttpServletRequest request) {
        if (log.isTraceEnabled()) {
            String headers = httpServletRequestHeadersToString(request);

            log.trace("Request: {}:{}[{}] -> {}:{}: headers: [{}], body.length: ", request.getRemoteAddr(), request.getRemotePort(),
                     request.getRemoteUser(), request.getMethod(), request.getContextPath() /* getPathInfo(), */, headers, request.getContentLengthLong());
        }
    }

    public static String httpServletRequestHeadersToString(HttpServletRequest request) {
        int headersCount = 0;
        StringBuilder headers = new StringBuilder();
        Enumeration<String> headerNames = request.getHeaderNames();
        
        headers.append("{");
        
        while (headerNames.hasMoreElements()) {
            if (headersCount > 0) {
                headers.append(", ");
            }
            headersCount++;
            String hdrName = headerNames.nextElement();
            String hdrValue = request.getHeader(hdrName);

            headers.append(hdrName).append("=").append(hdrValue);
        }
        headers.append("}");
        return headers.toString();
    }

    public static String httpHeadersToString(HttpHeaders httpHeaders) {
        return httpHeaders.toSingleValueMap().toString();
    }

    public static void logRequest(Logger log, HttpRequest request) {
        if (log.isTraceEnabled()) {
            String headers = httpHeadersToString(request.getHeaders());
            log.trace("Request[{}]: {} -> {}: headers: [{}],", request.hashCode(), request.getMethod(), request.getURI(), headers);
        }
    }

    public static String logResponseEntityToString(ResponseEntity<String> response) {
        return response.getStatusCodeValue() + ": '" + response.getBody() + "'";
    }
}
