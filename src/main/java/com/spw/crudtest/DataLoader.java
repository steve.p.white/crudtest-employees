package com.spw.crudtest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import com.spw.crudtest.config.WebSecurityConfig;
import com.spw.crudtest.domain.Employee;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);

    private final EmployeeRepository employeeRepository;

    @Autowired
    public DataLoader(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        try {
            SecurityContextHolder.getContext().setAuthentication(WebSecurityConfig.getAuthenticationAdmin());
            
            loadDefaultEmployees();
            
            SecurityContextHolder.clearContext();
        } catch (Exception ex) {
            log.error("Problem loading data: {}", ExceptionUtils.getStackTrace(ex));
            throw ex;
        }
    }

    private void loadDefaultEmployees() throws Exception {
        String resourcePath = "/data/employees-5.csv";

        log.trace("Loading employees from: {}", resourcePath);
        try (InputStreamReader inputReader = new InputStreamReader(this.getClass().getResourceAsStream(resourcePath), StandardCharsets.UTF_8)) {
            loadEmployees(this.employeeRepository, inputReader);
        }
    }
    
    public static void loadEmployees(CrudRepository<Employee,Long> repo, InputStreamReader inputReader) throws IOException {
        CSVFormat csvFormat = CSVFormat.DEFAULT.withFirstRecordAsHeader();
        
        Iterable<CSVRecord> records = csvFormat.parse(inputReader);

        int loadCount = 0;
        for (CSVRecord record : records) {
            String firstName = record.get(Employee.FIELD_FIRSTNAME); // we could do some clever pojo mapping here... 
            String lastName = record.get(Employee.FIELD_LASTNAME);
            
            Employee employee = new Employee(firstName, lastName);
            employee.setNewRecord();
            employee.setTimeUpdatedNow();
            
            Employee savedEmployee = repo.save(employee);
            if (log.isTraceEnabled()) {
                log.trace("Loaded employee: {}", savedEmployee);
            }
            
            loadCount++;
        }
        log.trace("Loaded employees: {}", loadCount);
    }
}
