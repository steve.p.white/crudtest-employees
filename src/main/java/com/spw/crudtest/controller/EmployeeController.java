package com.spw.crudtest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spw.crudtest.EmployeeRepository;
import com.spw.crudtest.Util;
import com.spw.crudtest.domain.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ExposesResourceFor(Employee.class)
@BasePathAwareController
@RequestMapping(value = "/employees")
@RepositoryRestController
public class EmployeeController {
    private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);

    private final EmployeeRepository repository;
    
    @Autowired
    public EmployeeController(EmployeeRepository repository,
                              PagedResourcesAssembler<Object> pagedResourcesAssembler) {
        this.repository = repository;
    }
    
    @RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody PersistentEntityResource post(HttpServletRequest request, @RequestBody Resource<Employee> newEntityResource,
                                                       PersistentEntityResourceAssembler resourceAssembler) {
        Util.logRequest(log, request);

        Employee newEmployee = newEntityResource.getContent();
        newEmployee.setNewRecord();

        Employee savedEntity = repository.save(newEmployee);

        return resourceAssembler.toResource(savedEntity);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })
    public @ResponseBody PersistentEntityResource put(HttpServletRequest request, @PathVariable("id") Long id, @RequestBody Resource<Employee> newEntityResource,
                                                      PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response) {
        Util.logRequest(log, request);

        Employee existingEmployee = repository.findOne(id);
        if (existingEmployee != null) {

            Employee newEmployee = newEntityResource.getContent();
            BeanUtils.copyProperties(newEmployee, existingEmployee, Employee.HEADER_FIELDS); // ignore HEADER_FIELDS e.g: "timeAdded", "version", "id"

            existingEmployee.setTimeUpdatedNow();

            Employee savedEntity = repository.save(existingEmployee);
            response.setStatus(HttpServletResponse.SC_CREATED);
            return resourceAssembler.toResource(savedEntity);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })
    public @ResponseBody PersistentEntityResource get(HttpServletRequest request, @PathVariable("id") Long id,
                                                      PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response) {
        Util.logRequest(log, request);

        Employee existingEmployee = repository.findOne(id);
        if (existingEmployee != null) {
            if (log.isTraceEnabled()) {
                log.trace("Found: {}", existingEmployee);
            }
            existingEmployee.setRecordId(existingEmployee.getId());

            return resourceAssembler.toResource(existingEmployee);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @RequestMapping(path = "/", method = RequestMethod.DELETE)
    public @ResponseBody PersistentEntityResource deleteAll(HttpServletRequest request,
                                                            PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response) {
        repository.deleteAll();
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return null;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody PersistentEntityResource delete(HttpServletRequest request, @PathVariable("id") Long id,
                                                         PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response) {
        Util.logRequest(log, request);
        try {
            repository.delete(id);
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } catch (EmptyResultDataAccessException ex) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return null;
    }
}
