package com.spw.crudtest.domain

import java.util.Date
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

import javax.persistence.Version
import javax.persistence.Column;

import org.apache.commons.lang3.builder.EqualsBuilder
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include
import org.hibernate.validator.constraints.Length
import java.util.Objects
import javax.persistence.GenerationType
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
@SuppressFBWarnings("MS_MUTABLE_ARRAY")
class Employee : Record {
		
	var recordId: Long? = null
	
	@Length(min = 1, max = 32)
	@Column(nullable = false)
	var firstName: String? = null
	
	@Length(min = 1, max = 32)
	@Column(nullable = false)
	var lastName: String? = null

	constructor() {}

	constructor(firstName: String?, lastName: String?) {
		this.firstName = firstName
		this.lastName = lastName
	}

	companion object {
		@JvmField val FIELD_FIRSTNAME: String? = "firstName"
		@JvmField val FIELD_LASTNAME: String? = "lastName"
		
		@JvmField var HEADER_FIELDS: Array<String> = arrayOf("id" , "version", "timeAdded" )
	}

	fun setTimeUpdatedNow() {
		this.timeUpdated = Date().time
	}

	fun setNewRecord() {
		var timeNow = Date().time;

		timeAdded = timeNow
		timeUpdated = timeNow
	}

	fun setId() {
		recordId = id;
	}
	
	fun equalsRecord(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || javaClass != other.javaClass) return false
		val that = other as Employee?
		return firstName == that?.firstName &&
				lastName == that!!.lastName &&
				id == that.id &&
				timeAdded == that.timeAdded &&
				timeUpdated == that.timeUpdated
	}

	override fun equalsData(obj: Any?): Boolean {
		if (this === obj) return true
		if (obj == null || javaClass != obj.javaClass) return false
		val that = obj as Employee?
		return firstName == that?.firstName &&
				lastName == that!!.lastName
	}
}