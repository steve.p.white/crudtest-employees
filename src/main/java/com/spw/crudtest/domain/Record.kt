package com.spw.crudtest.domain

import org.apache.commons.lang3.builder.ToStringBuilder
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.GenerationType
import javax.persistence.MappedSuperclass
import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Version
import javax.persistence.Column;

@MappedSuperclass
open class Record {
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO) var id: Long? = null
	
	@Version @JsonIgnore var version: Long? = null

	@Column(nullable = false)
	var timeAdded: Long? = null
	
	@Column(nullable = false)
	var timeUpdated: Long? = null
	
	@Transient var _links: Map<String,Any>? = null // transient - cause hibernate to ignore this
	
	open fun equalsData(obj: Any?): Boolean {
		return false;
	}
	
	override fun toString() : String {
		return ToStringBuilder.reflectionToString(this);
	}
}