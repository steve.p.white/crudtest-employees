package com.spw.crudtest;

import com.spw.crudtest.domain.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {
    
    static final Logger log = LoggerFactory.getLogger(EmployeeRepository.class);
    
    @Override
    default Page<Employee> findAll(Pageable p) {
        if (p.getSort() == null) {   
            return findBy(new PageRequest(p.getPageNumber(), p.getPageSize(), Sort.Direction.DESC, "id")); // default sort order: id descending
        }
        return findBy(p);
    }
    
    @RestResource(exported = false)
    Page<Employee> findBy(Pageable pageable);
}