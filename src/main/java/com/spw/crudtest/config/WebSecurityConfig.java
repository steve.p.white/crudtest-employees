package com.spw.crudtest.config;

import java.util.Base64;
import java.nio.charset.StandardCharsets;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String ADMIN_USER = "admin";
    public static final String ADMIN_PASS = new String(Base64.getDecoder().decode("Y3J1ZHRlc3Q="), StandardCharsets.UTF_8); // simple obfuscation...
    
    static final String[] ADMIN_ROLES = new String[] {
            "ADMIN",
            "ACTUATOR" }; // allows access health/metrics

    public static Authentication getAuthenticationAdmin() {
        return new UsernamePasswordAuthenticationToken(ADMIN_USER, ADMIN_PASS,
                                                       AuthorityUtils.createAuthorityList(ADMIN_ROLES));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser(ADMIN_USER).password(ADMIN_PASS).roles(ADMIN_ROLES);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .frameOptions()
                .sameOrigin(); // Disable X-Frame-Options header for same origin requests
        http.authorizeRequests()
                .antMatchers("/built/**", "/main.css", "/info").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .defaultSuccessUrl("/", true)
                .permitAll()
                .and()
                .httpBasic()
                .and()
                .csrf().disable()
                .logout()
                .logoutSuccessUrl("/");
    }

}
